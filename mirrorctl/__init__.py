import sys

__version__ = "0.1"

def run_as_a_module():
	from .cli.argument_router import arguments
	from .cli.global_args import parser
	
	if 'func' in dir(arguments):
		arguments.func(arguments)
	else:
		parser.print_help(sys.stderr)