import argparse

parser = argparse.ArgumentParser("mirrorctl")
parser.add_argument(
	"--verbose",
	required=False,
	action="store_true",
	default=False,
	help="Increase verbosity",
)

subparsers = parser.add_subparsers(help="Sub-commands help")