from .global_args import parser, subparsers
from .arguments.sign import sign_arguments
from .arguments.identity import identity_arguments

arguments, _ = parser.parse_known_args()