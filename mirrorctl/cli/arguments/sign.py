import pathlib
from ..global_args import subparsers
from ...modules.sign.sign_entrypoint import sign

sign_arguments = subparsers.add_parser("sign", help="Performs signing operations on a mirror")
sign_arguments.add_argument(
	"--certificate",
	required=True,
	type=pathlib.Path,
	help="Which x509 certificate to use for signing",
)
sign_arguments.add_argument(
	"--privkey",
	required=True,
	type=pathlib.Path,
	help="Which x509 private key to use for signing",
)
sign_arguments.set_defaults(func=sign)