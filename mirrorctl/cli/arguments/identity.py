import pathlib
from ..global_args import subparsers
from ...modules.identity.identity_entrypoint import identity

identity_arguments = subparsers.add_parser("identity", help="Create and manage a certificate identity")
identity_arguments.add_argument(
	"--certificate",
	required=True,
	type=pathlib.Path,
	help="Where to store the certificate",
)
identity_arguments.add_argument(
	"--privkey",
	required=True,
	type=pathlib.Path,
	help="Where to store the private key",
)
identity_arguments.add_argument(
	"--days",
	default=365,
	type=int,
	help="How many days should the certificate be valid",
)
identity_arguments.set_defaults(func=identity)