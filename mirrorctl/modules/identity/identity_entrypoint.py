import argparse
import subprocess
import shutil
import tempfile
import pathlib
import re

def identity(args :argparse.Namespace) -> None:
	"""
	This module can help create a best practice x509 certificate and privkey using EllipticCurve P-384.
	The certificate (or public key thereof) can be uploaded to the `mirror specification`_ on the `mirror project`_ at `GitLab`_.

	- Workflow is copied from https://gitlab.archlinux.org/archlinux/archiso/-/blob/965da3379105d44bf15ceb54f84a3d08d8d7ae04/.gitlab/ci/build_archiso.sh

	:param args: The command line arguments.
	:type args: argparse.Namespace

	:raises ValueError: Exception is raised when --privkey or --certificate exists.
	:return: None
	:rtype: None
	"""

	if args.privkey.exists():
		raise ValueError(f"--privkey already exists: {args.privkey}")
	if args.certificate.exists():
		raise ValueError(f"--certificate already exists: {args.certificate}")

	with tempfile.TemporaryDirectory() as tmpdirname:
		codesigning_dir = pathlib.Path(f"{tmpdirname}/.codesigning/")
		ca_dir = codesigning_dir / "ca"

		ca_conf = ca_dir / "certificate_authority.cnf"
		ca_subj = '/C=DE/ST=Berlin/L=Berlin/O=Arch Linux/OU=Mirror Administrators/emailAddress=mirrors@archlinux.org/CN=Arch Linux Mirror Administrators (Ephemeral CA)'
		ca_cert = ca_dir / "cacert.pem"
		ca_key = ca_dir / "private" / "cakey.pem"

		codesigning_conf = codesigning_dir / "code_signing.cnf"
		codesigning_subj = '/C=DE/ST=Berlin/L=Berlin/O=Arch Linux/OU=Mirror Administrators/emailAddress=mirrors@archlinux.org/CN=Arch Linux Mirror Administrators (Ephemeral Signing Key)'

		for subdir in ['private', 'newcerts', 'crl']:
			(ca_dir / subdir).mkdir(parents=True)

		shutil.copy2("/etc/ssl/openssl.cnf", str(codesigning_conf))
		shutil.copy2("/etc/ssl/openssl.cnf", str(ca_conf))
		(ca_dir / "index.txt").touch()
		with (ca_dir / "serial").open("w") as serial:
			serial.write("1000")

		# Prepare the ca configuration for the change in directory
		with ca_conf.open("r") as fh:
			lines = fh.readlines()
		with ca_conf.open("w") as fh:
			for line in lines:
				fh.write(re.sub(r'/etc/ssl', str(ca_dir), line))

		# Create the Certificate Authority
		subprocess.call([
			"openssl", "req",
			"-newkey", "ec", "-pkeyopt", 'ec_paramgen_curve:P-384',
			"-sha256",
			"-nodes",
			"-x509",
			"-new",
			"-sha256",
			"-keyout", str(ca_key),
			"-config", str(ca_conf),
			"-subj", str(ca_subj),
			"-days", str(args.days),
			"-out", str(ca_cert),
		])

		extension_text = """
[codesigning]
keyUsage=digitalSignature
extendedKeyUsage=codeSigning, clientAuth, emailProtection
"""

		with ca_conf.open("a") as fh:
			fh.write(extension_text)
		with codesigning_conf.open("a") as fh:
			fh.write(extension_text)

		subprocess.call([
			"openssl", "req",
			"-newkey", "ec", "-pkeyopt", "ec_paramgen_curve:P-384",
			"-keyout", str(args.privkey),
			"-nodes",
			"-sha256",
			"-out", f"{args.certificate}.csr",
			"-config", str(codesigning_conf),
			"-subj", str(codesigning_subj),
			"-extensions", "codesigning"
		])

		# Sign the code signing certificate with the CA
		subprocess.call([
			"openssl", "ca",
			"-batch",
			"-config", str(ca_conf),
			"-extensions", "codesigning",
			"-days", str(args.days),
			"-notext",
			"-md", "sha256",
			"-in", f"{args.certificate}.csr",
			"-out", f"{args.certificate}"
		])
	