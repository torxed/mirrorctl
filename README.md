## mirrorctl

A tool to create, maintain and administrate Arch Linux mirrors related activities.
The tool is a reference implementation for the [Arch Linux Mirror Specification](https://gitlab.archlinux.org/archlinux/arch-mirrors).